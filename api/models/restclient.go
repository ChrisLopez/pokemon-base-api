package models

type Response struct{
	Info Info `json:"info,omitempty"`
	Content interface{} `json:"content,omitempty"`
}

type Info struct{
	Success bool `json:"success"`
	Title string `json:"title,omitempty"`
	Message string `json:"message,omitempty"`
}

type ValidationErrors struct{
	Errors []string `json:"errors,omitempty"`
}

type PokemonResponse struct{
	Pokemon []Pokemon `json:"pokemon_list,omitempty"`
}
type AverageResponse struct{
	AverageRegisters []AverageRegister `json:"average_registers,omitempty"`
}

