package restclient

import (
    "net/http"
	"chrisloops.com/api/controllers"
	"chrisloops.com/api/utils"
	"chrisloops.com/api/models"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func registerAverage(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	averageType := vars["average_type"]

	if(averageType!="weekly" && averageType != "monthly"){
		utils.ResponseNotFound(w, "We are sorry","There no exist the type of average you're looking for.");
		return
	}

	err := controllers.RegisterPokemonAverage(averageType)
	if(err != nil){
		utils.ResponseServerError(w)
		return
	}
	utils.ResponseOk(w, "Success", "Average recorded successfully.")
}

func getAllAverages(w http.ResponseWriter, r *http.Request){

	averageList, err := controllers.FindAverage()

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	allAverages := models.AverageResponse{AverageRegisters: averageList} 

	response := models.Response {
		Info : models.Info{Success: true},
		Content: allAverages,
	}

	w.Header().Set("Content-Type", "application/json")

	if(err != nil){
		utils.ResponseServerError(w)
	}

	utils.ResponseJson(w, response)
}

func getAllPokemon(w http.ResponseWriter, r *http.Request){

	pokemonList, err := controllers.FindPokemon()

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	allPokemonResponse := models.PokemonResponse{Pokemon: pokemonList} 

	response := models.Response {
		Info : models.Info{Success: true},
		Content: allPokemonResponse,
	}

	w.Header().Set("Content-Type", "application/json")

	if(err != nil){
		utils.ResponseServerError(w)
	}

	utils.ResponseJson(w, response)
}

func insertPokemon(w http.ResponseWriter, r *http.Request){
	NewPokemon := &models.SinglePokemon{}

	utils.ParseBody(r, NewPokemon)

	validate := validator.New()
	err := validate.Struct(NewPokemon)

	if(err != nil){
		validationErrors := err.(validator.ValidationErrors)

		if (len(validationErrors)>0) {
			utils.ResponseBadRequest(w, validationErrors)
			return
		}
	}

	err = controllers.InsertPokemon(NewPokemon)

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	utils.ResponseOk(w, "Successful registration", "The pokemon has been registered successfully.")
	return 
}

func updatePokemon(w http.ResponseWriter, r *http.Request){
	UpdatePokemon := &models.SinglePokemon{}

	vars := mux.Vars(r)
	pokemonID := vars["pokemon_id"]
	
	utils.ParseBody(r, UpdatePokemon)

	pokemon, err :=  controllers.GetPokemon(pokemonID)

	if(err != nil || pokemon.Id == primitive.NilObjectID){
		utils.ResponseNotFound(w, "Not Found", "We did not found the resource you're looking for.")
		return
	}

	validate := validator.New()
	err = validate.Struct(UpdatePokemon)
	if(err != nil){
		validationErrors := err.(validator.ValidationErrors)

		if (len(validationErrors)>0) {
			utils.ResponseBadRequest(w, validationErrors)
			return
		}
	}

	err = controllers.UpdatePokemon(pokemonID, UpdatePokemon)

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	utils.ResponseOk(w, "Successful update", "The pokemon has been updated successfully.")
	return 
}

func insertMove(w http.ResponseWriter, r *http.Request){
	NewMove := &models.SingleMove{}

	vars := mux.Vars(r)
	pokemonID := vars["id"]
	
	utils.ParseBody(r, NewMove)

	pokemon, err :=  controllers.GetPokemon(pokemonID)

	if(err != nil || pokemon.Id == primitive.NilObjectID){
		utils.ResponseNotFound(w, "Not Found", "We did not found the resource you're looking for.")
		return
	}

	validate := validator.New()
	err = validate.Struct(NewMove)
	if(err != nil){
		validationErrors := err.(validator.ValidationErrors)

		if (len(validationErrors)>0) {
			utils.ResponseBadRequest(w, validationErrors)
			return
		}
	}
	

	err = controllers.InsertMove(pokemonID, NewMove)

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	utils.ResponseOk(w, "Successful registration", "The pokemon move has been registered successfully.")
	return 
}

func updateMove(w http.ResponseWriter, r *http.Request){
	UpdateMove := &models.SingleMove{}

	vars := mux.Vars(r)
	pokemonID := vars["pokemon_id"]
	moveID := vars["move_id"]
	
	utils.ParseBody(r, UpdateMove)

	move, err :=  controllers.GetMove(pokemonID, moveID)

	if(err != nil || move.Id == primitive.NilObjectID){
		utils.ResponseNotFound(w, "Not Found", "We did not found the resource you're looking for.")
		return
	}

	validate := validator.New()
	err = validate.Struct(UpdateMove)
	if(err != nil){
		validationErrors := err.(validator.ValidationErrors)

		if (len(validationErrors)>0) {
			utils.ResponseBadRequest(w, validationErrors)
			return
		}
	}
	

	err = controllers.UpdateMove(pokemonID, moveID, UpdateMove)

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	utils.ResponseOk(w, "Successful update", "The pokemon move has been updated successfully.")
	return 
}

func insertLearningWay(w http.ResponseWriter, r *http.Request){
	NewLearningWay := &models.SingleLearningWay{}

	vars := mux.Vars(r)
	pokemonID := vars["pokemon_id"]
	moveID := vars["move_id"]
	
	utils.ParseBody(r, NewLearningWay)

	move, err :=  controllers.GetMove(pokemonID, moveID)

	if(err != nil || move.Id == primitive.NilObjectID){
		utils.ResponseNotFound(w, "Not Found", "We did not found the resource you're looking for.")
		return
	}

	validate := validator.New()
	err = validate.Struct(NewLearningWay)
	if(err != nil){
		validationErrors := err.(validator.ValidationErrors)

		if (len(validationErrors)>0) {
			utils.ResponseBadRequest(w, validationErrors)
			return
		}
	}
	

	err = controllers.InsertLearningWay(pokemonID, moveID, NewLearningWay)

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	utils.ResponseOk(w, "Successful registration", "The pokemon learning way move has been registered successfully.")
	return 
}

func updateLearningWay(w http.ResponseWriter, r *http.Request){
	UpdateLearningWay := &models.SingleLearningWay{}

	vars := mux.Vars(r)
	pokemonID := vars["pokemon_id"]
	moveID := vars["move_id"]
	learningID := vars["learning_id"]

	utils.ParseBody(r, UpdateLearningWay)

	learning_way,index, err :=  controllers.GetLearningWay(pokemonID, moveID, learningID)

	if(err != nil || learning_way.Id == primitive.NilObjectID){
		utils.ResponseNotFound(w, "Not Found", "We did not found the resource you're looking for.")
		return
	}

	validate := validator.New()
	err = validate.Struct(UpdateLearningWay)
	if(err != nil){
		validationErrors := err.(validator.ValidationErrors)

		if (len(validationErrors)>0) {
			utils.ResponseBadRequest(w, validationErrors)
			return
		}
	}
	

	err = controllers.UpdateLearningWay(pokemonID, moveID,learningID, index, UpdateLearningWay)

	if(err != nil){
		utils.ResponseServerError(w)
		return
	}

	utils.ResponseOk(w, "Successful update", "The pokemon learning way move has been updated successfully.")
	return 
}