package restclient

import (
    "log"
    "net/http"
    "github.com/gorilla/mux"
)



func HandleRequests() {

    muxRouter := mux.NewRouter().StrictSlash(true)

    /*Average Service*/
    muxRouter.HandleFunc("/averages/{average_type}", registerAverage).Methods("POST")
    muxRouter.HandleFunc("/averages", getAllAverages).Methods("GET")

    /*Pokemon Services*/
    muxRouter.HandleFunc("/pokemon", getAllPokemon).Methods("GET")
    muxRouter.HandleFunc("/pokemon", insertPokemon).Methods("POST")
    muxRouter.HandleFunc("/pokemon/{pokemon_id}", updatePokemon).Methods("PUT")

    /*Moves Services*/
    muxRouter.HandleFunc("/pokemon/{id}/moves", insertMove).Methods("POST")
    muxRouter.HandleFunc("/pokemon/{pokemon_id}/moves/{move_id}", updateMove).Methods("PUT")

    /*Learning ways Services*/
    muxRouter.HandleFunc("/pokemon/{pokemon_id}/moves/{move_id}/learning_ways", insertLearningWay).Methods("POST")
    muxRouter.HandleFunc("/pokemon/{pokemon_id}/moves/{move_id}/learning_ways/{learning_id}", updateLearningWay).Methods("PUT")

    log.Fatal(http.ListenAndServe(":8080", muxRouter))
}