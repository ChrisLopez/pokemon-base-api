package controllers


import (
	"context"
	"chrisloops.com/api/dbintegration"
	"chrisloops.com/api/models"
	"log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"fmt"
	"time"
	"github.com/jinzhu/now"
)

func GetPokemon(id string) (models.Pokemon, error){

	pokemon := models.Pokemon{}
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  pokemon, err
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")

	objID, _ := primitive.ObjectIDFromHex(id)
	err = pokemonCollection.FindOne(context.TODO(), bson.M{"_id": objID}).Decode(&pokemon)

	if err != nil {
		return pokemon, err
	}

	return pokemon, nil
}

func FindPokemon()([]models.Pokemon, error){

	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return nil, err
	}

	pokemonList := []models.Pokemon{}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")

	cursor, err := pokemonCollection.Find(context.TODO(), bson.D{})

	if err != nil {
		return nil, err
	}

	defer cursor.Close(context.TODO())
	for cursor.Next(context.TODO()) {
		var pokemon models.Pokemon
		if err = cursor.Decode(&pokemon); err != nil {
			log.Fatal(err)
		}
		pokemonList = append(pokemonList, pokemon)
	}

	return pokemonList, nil
}

func InsertPokemon(pokemon *models.SinglePokemon)( error){
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  err
	}

	pokemon.CreatedAt = time.Now()

	pokemonCollection := client.Database("pokemon").Collection("pokemon")

	_, err = pokemonCollection.InsertOne(context.TODO(), pokemon)

	if err != nil {

		log.Fatal(err)

		return  err

	}

	return nil
}

func UpdatePokemon(pID string, pokemon *models.SinglePokemon)( error){
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  err
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")

	update := bson.M{
		"$set": bson.M{"name": pokemon.Name, "description":pokemon.Description, "weigth":pokemon.Weigth, "height": pokemon.Height},
	}
	
	pokemonID, _ := primitive.ObjectIDFromHex(pID)

	_, err = pokemonCollection.UpdateOne(context.Background(), bson.M{"_id": pokemonID}, update)

	if err != nil {

		log.Fatal(err)

		return  err

	}

	return nil
}

func GetMove(pID string, mID string) (models.Move, error){
	move := models.Move{}
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  move, err
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")

	pokemonID, _ := primitive.ObjectIDFromHex(pID)
	moveID, _ := primitive.ObjectIDFromHex(mID)

	unwindStage := bson.D{{"$unwind","$moves"}}
	matchStage := bson.D{{"$match", bson.D{{"_id", pokemonID},{"moves._id", moveID}}}}
	projectStage := bson.D{{"$project", bson.D{{"_id","$moves._id"},{"name", "$moves.name"},{"description", "$moves.description"},{"learning_ways","$moves.learning_ways"}}}}


	moveSelected,err := pokemonCollection.Aggregate(context.TODO(), mongo.Pipeline{unwindStage,matchStage,projectStage})

	if err != nil {
		log.Println(err)
		return move, err
	}


	defer moveSelected.Close(context.TODO())
	for moveSelected.Next(context.TODO()) {
		if err = moveSelected.Decode(&move); err != nil {
			return move, err
		}
	}
	

	return move, nil
}

func InsertMove(pokemonID string, move *models.SingleMove)(error){
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  err
	}
	
	move.Id = primitive.NewObjectID()
	move.CreatedAt = time.Now()

	update := bson.M{
		"$push": bson.M{"moves": move},
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")
	
	objID, _ := primitive.ObjectIDFromHex(pokemonID)

	_, err = pokemonCollection.UpdateOne(context.Background(), bson.M{"_id": objID}, update)

	if(err != nil){
		return err;
	}

	return nil
}

func UpdateMove(pID string, mID string, move *models.SingleMove)(error){
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  err
	}
	

	update := bson.M{
		"$set": bson.M{"moves.$.name": move.Name,"moves.$.description": move.Description },
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")
	
	pokemonID, _ := primitive.ObjectIDFromHex(pID)
	moveID, _ := primitive.ObjectIDFromHex(mID)

	_, err = pokemonCollection.UpdateOne(context.Background(), bson.M{"_id": pokemonID, "moves._id": moveID}, update)

	if(err != nil){
		return err;
	}

	return nil
}

func UpdateRecords(begin time.Time, end time.Time)(error){
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  err
	}
	
	auditedAt := time.Now()

	update := bson.M{
		"$set": bson.M{"audited_at": auditedAt,"moves.$[].audited_at": auditedAt },
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")
	

	_, err = pokemonCollection.UpdateMany(context.Background(), bson.M{"created_at": bson.M{ "$gt":begin, "$lt":end}}, update)

	if(err != nil){
		return err;
	}

	return nil
}

func GetLearningWay(pID string, mID string,lID string) (models.LearningWay, int, error){
	move, err := GetMove(pID, mID)
	learningWay := models.LearningWay{}

	if(err != nil){
		return learningWay, 0, err
	}
	

	learningID, _ := primitive.ObjectIDFromHex(lID)

	for index, learningWayLoop := range move.LearningWays {
		if(learningWayLoop.Id == learningID){
			return learningWayLoop, index, nil
		}
	}

	return learningWay,0, nil
}

func InsertLearningWay(pID string,mID string, learningWay *models.SingleLearningWay)(error){
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  err
	}
	
	learningWay.Id = primitive.NewObjectID()
	learningWay.CreatedAt = time.Now()

	update := bson.M{
		"$push": bson.M{"moves.$.learning_ways": learningWay},
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")
	
	pokemonID, _ := primitive.ObjectIDFromHex(pID)
	moveID, _ := primitive.ObjectIDFromHex(mID)

	_, err = pokemonCollection.UpdateOne(context.Background(), bson.M{"_id": pokemonID,"moves._id" : moveID,}, update)

	if(err != nil){
		return err;
	}

	return nil
}

func UpdateLearningWay(pID string,mID string,lID string,index int, learningWay *models.SingleLearningWay)(error){
	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return  err
	}
	
	update := bson.M{
		"$set": bson.M{fmt.Sprintf("moves.$.learning_ways.%d.name", index): learningWay.Name, fmt.Sprintf("moves.$.learning_ways.%d.description", index): learningWay.Description},
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")
	
	pokemonID, _ := primitive.ObjectIDFromHex(pID)
	moveID, _ := primitive.ObjectIDFromHex(mID)
	learningID, _ := primitive.ObjectIDFromHex(lID)

	_, err = pokemonCollection.UpdateOne(context.Background(), bson.M{"_id": pokemonID,"moves._id" : moveID,"moves.learning_ways._id":learningID}, update)

	if(err != nil){
		log.Print(err)
		return err;
	}

	return nil
}

func RegisterPokemonAverage(registerType string)(error){
	
	client, err := dbintegration.Connect()
	defer client.Disconnect(context.TODO())

	var begin time.Time
	var end time.Time

	if(registerType == "weekly"){
		begin = now.BeginningOfWeek()
		end = now.EndOfWeek()
	}else if(registerType == "monthly"){
		begin = now.BeginningOfMonth()
		end = now.EndOfMonth()
	}
	
	if(err != nil){
		log.Print(err)
		return err
	}

	pokemonCollection := client.Database("pokemon").Collection("pokemon")

	groupStage := bson.D{
		{
			"$group", bson.D{
				{"_id",""},
				{"average_weigth", bson.D{{"$avg", "$weigth"}}}, 
				{"avgHeigth", bson.D{{"$avg", "$height"}}},
				{"registers_amount", bson.D{{"$sum",1}}},
			},
		},
	}
	
	matchStage := bson.D{{"$match",bson.D{{"created_at", bson.D{{ "$gt", begin}, {"$lt", end}} }} }}
	
	resultSelected, err := pokemonCollection.Aggregate(context.TODO(), mongo.Pipeline{matchStage,groupStage})

	if(err != nil){
		fmt.Println(err)
		return  nil
	}

	var averageRegister = models.SingleAverageRegister{}

	defer resultSelected.Close(context.TODO())
	for resultSelected.Next(context.TODO()) {
		if err = resultSelected.Decode(&averageRegister); err != nil {
			return err
		}
	}
	
	averageRegister.Type = registerType
	averageRegister.StartedAt = begin
	averageRegister.EndedAt = end
	averageRegister.CreatedAt = time.Now()


	averageCollection := client.Database("pokemon").Collection("averages")
	_, err = averageCollection.InsertOne(context.TODO(), averageRegister)

	if err != nil {

		log.Fatal(err)

		return  err

	}

	err = UpdateRecords(begin, end)

	if(err != nil){
		fmt.Println(err)
	}

	return nil
}

func FindAverage()([]models.AverageRegister, error){

	client, err := dbintegration.Connect()

	defer client.Disconnect(context.TODO())
	if(err != nil){
		log.Print(err)
		return nil, err
	}

	averageList := []models.AverageRegister{}

	averageCollection := client.Database("pokemon").Collection("averages")

	cursor, err := averageCollection.Find(context.TODO(), bson.D{})

	if err != nil {
		return nil, err
	}

	defer cursor.Close(context.TODO())
	for cursor.Next(context.TODO()) {
		var average models.AverageRegister
		if err = cursor.Decode(&average); err != nil {
			log.Fatal(err)
		}
		averageList = append(averageList, average)
	}

	return averageList, nil
}