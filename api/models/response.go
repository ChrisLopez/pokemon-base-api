package models

import(
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Pokemon struct{
	Id primitive.ObjectID `bson:"_id" json:"id"`
	Name string `bson:"name,omitempty" json:"name,omitempty"`
	Weigth  float32 `bson:"weigth,omitempty" json:"weigth,omitempty"`
	Height  float32 `bson:"height,omitempty" json:"height,omitempty"`
	Description string `bson:"description,omitempty" json:"description,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
	AuditedAt time.Time `bson:"audited_at,omitempty" json:"audited_at,omitempty"`
	Moves []Move `bson:"moves,omitempty" json:"moves,omitempty"`
}

type Move struct{
	Id primitive.ObjectID `bson:"_id" json:"id"`
	Name string `bson:"name,omitempty" json:"name,omitempty"`
	Description string `bson:"description,omitempty" json:"description,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
	AuditedAt time.Time `bson:"audited_at,omitempty" json:"audited_at,omitempty"`
	LearningWays []LearningWay `bson:"learning_ways,omitempty" json:"learning_ways,omitempty"`
}

type LearningWay struct{
	Id primitive.ObjectID `bson:"_id" json:"id"`
	Name string `bson:"name,omitempty" json:"name,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
	AuditedAt time.Time `bson:"audited_at,omitempty" json:"audited_at,omitempty"`
	Description string `bson:"description,omitempty" json:"description,omitempty"`
}

type AverageRegister struct{
	Id primitive.ObjectID `bson:"_id" json:"id"`
	Weigth  float32 `bson:"average_weigth,omitempty" json:"average_weigth,omitempty"`
	Height  float32 `bson:"average_height,omitempty" json:"average_height,omitempty"`
	Count int `bson:"registers_amount,omitempty" json:"registers_amount,omitempty"`
	Type string `bson:"type,omitempty" json:"type,omitempty"`
	StartedAt time.Time `bson:"started_at,omitempty" json:"started_at,omitempty"`
	EndedAt time.Time `bson:"ended_at,omitempty" json:"ended_at,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
}