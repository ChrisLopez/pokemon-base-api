# Pokemon Base Api
Es un api desarrollada en Go que ofrece servicios básicos de mantenimiento a una entidad con 3 niveles de profundidad.

Como se menciono el Api esta desarrollada en Go y se conecta a una base de datos NoSql documental en este caso MongoDB.



## 1. Entidad
La entidad se realizo apta para 3 profundidades, se realizo el modelado en base a un listado de pokemon que tienen diferentes movimientos y estos movimientos a su vez un listado de maneras de aprendizaje.

Las relaciones entre entidades se hicieron de manera embedida asumiendo que a nuestra aplicación lo que más le interesa es el listado de pokemon y no tanto los movimientos y maneras de aprendizaje. Por eso las relaciones se hicieron de forma embedida en los pokemon.

![Entidad](/entidad.jpg)

## 2 API

## 2.1 Códigos HTTP y respuestas por defecto

### 200 HTTP OK

Respuesta por defecto de un request exitoso.

```json
{
    "info": {
        "success": true,
        "title": "Successful registration",
        "message": "The pokemon move has been registered successfully."
    }
}
```

| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| info |  Array que contiene la información básica del request. | - |
| info.success |    Valor que indica si el request fue exitoso o no dentro del sistema.   |   - |
| info.title | Contiene un título general de la acción realizada. |    - |
| info.message | Contiene un mensaje general de la acción realizada. |    - |


### 500 HTTP INTERNAL SERVER ERROR

Respuesta por defecto cuando el servidor falle al momento de procesar el request.

```json
{
    "info": {
        "success": false,
        "title": "We are sorry",
        "message": "There was an error which we can't handle right now, please try again later."
    }
}
```

| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| info |  Array que contiene la información básica del request. | - |
| info.success |    Valor que indica si el request fue exitoso o no dentro del sistema.   |   - |
| info.title | Contiene un título general de la acción realizada. |    - |
| info.message | Contiene un mensaje general de la acción realizada. |    - |


### 404 HTTP NOT FOUND

Respuesta por defecto cuando no se encuentra el recurso que se busca.

```json
{
    "info": {
        "success": false,
        "title": "Not Found",
        "message": "We did not found the resource you're looking for."
    }
}
```

| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| info |  Array que contiene la información básica del request. | - |
| info.success |    Valor que indica si el request fue exitoso o no dentro del sistema.   |   - |
| info.title | Contiene un título general de la acción realizada. |    - |
| info.message | Contiene un mensaje general de la acción realizada. |    - |


### 400 HTTP BAD REQUEST

Respuesta por defecto cuando el request no cumple con la estructura definida.

```json
{
    "info": {
        "success": false,
        "title": "There's was something wrong",
        "message": "Please check the data in your request."
    },
    "content": {
        "errors": [
            "Key: 'SingleMove.Name' Error:Field validation for 'Name' failed on the 'required' tag"
        ]
    }
}

```
| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| info |  Array que contiene la información básica del request. | - |
| info.success |    Valor que indica si el request fue exitoso o no dentro del sistema.   |   - |
| info.title | Contiene un título general de la acción realizada. |    - |
| info.message | Contiene un mensaje general de la acción realizada. |    - |
| content |  Array que contiene la información del request. | - |
| content.error |    Array de strings que indican los errores en el request.   |   - |


## 2.2 Pokemon

#### OBTENER POKEON

Método : GET

EndPoint: http://localhost:8081/pokemon

***Response***
```json
{
    "info": {
        "success": true
    },
    "content": {
        "pokemon_list": [
            {
                "id": "600158d0614f7acd76ea4644",
                "name": "Squirtle",
                "weigth": 58,
                "height": 15,
                "description": "A pokemon that looks like a turtle",
                "created_at": "0001-01-01T00:00:00Z",
                "audited_at": "0001-01-01T00:00:00Z",
                "moves": [
                    {
                        "id": "600159d605bfe367b8e60997",
                        "name": "Acupressure",
                        "description": "Sharply raises a random stat.",
                        "created_at": "0001-01-01T00:00:00Z",
                        "audited_at": "0001-01-01T00:00:00Z",
                        "learning_ways": [
                            {
                                "id": "60015a5605bfe367b8e60998",
                                "name": "MT",
                                "created_at": "0001-01-01T00:00:00Z",
                                "audited_at": "0001-01-01T00:00:00Z",
                                "description": "The pokemon is able to learn this move by using a MT"
                            }
                        ]
                    }
                ]
            }
        ]
    }
}
```

| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| info |  Array que contiene la información básica del request. | - |
| info.success |    Valor que indica si el request fue exitoso o no dentro del sistema.   |   - |
| content |  Array que contiene la información del request. | - |
| content.pokemon_list |    Array que contiene todos los registros de pokemon  |   - |
| content.pokemon_list.id |    Identificador del pokemon  |   - |
| content.pokemon_list.name |    Nombre del pokemon  |   - |
| content.pokemon_list.weigth |    Peso del pokemon  |   - |
| content.pokemon_list.height |    Altura del pokemon  |   - |
| content.pokemon_list.description |    Descripción del pokemon  |   - |
| content.pokemon_list.created_at |    Fecha de creación del pokemon  |   - |
| content.pokemon_list.audited_at |    Fecha de registro en listado de promedios del pokemon  |   - |
| content.pokemon_list.moves.id |    Identificador del movimiento  |   - |
| content.pokemon_list.moves.id |    Nombre del movimiento  |   - |
| content.pokemon_list.moves.description |    Descripción del movimiento  |   - |
| content.pokemon_list.moves.created_at |    Fecha de creación del movimiento  |   - |
| content.pokemon_list.moves.audited_at |    Fecha de registro en listado de promedios del movimiento  |   - |
| content.pokemon_list.moves.learning_ways.id |    Identificador del movimiento  |   - |
| content.pokemon_list.moves.learning_ways.id |    Nombre de la manera de aprendizaje  |   - |
| content.pokemon_list.moves.learning_ways.description |    Descripción del movimiento  |   - |
| content.pokemon_list.moves.learning_ways.created_at |    Fecha de creación de la manera de aprendizaje  |   - |
| content.pokemon_list.moves.learning_ways.audited_at |    Fecha de registro en listado de promedios de la manera de aprendizaje  |   - |

#### CREAR POKEMON

Método : POST

EndPoint: http://localhost:8081/pokemon

***Request***
```json
{
	"name": "Bulbasaurl",
	"weigth": 96.8,
	"height": 58,
	"description": "A pokemon which is green an heavy"
}
```


| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| name |    Nombre del pokemon  |  requerido |
| weigth |    Peso del pokemon  |   requerido |
| height |    Altura del pokemon  |   requerido |
| description |    Descripción del pokemon  |   opcional |


***Response***

Dependiendo el request se dará una respuesta http por defecto.


#### ACTUALIZAR POKEMON

Método : PUT

EndPoint: http://localhost:8081/pokemon/{pokemon_id}

***Request***
```json
{
	"name": "Bulbasaurl",
	"weigth": 96.8,
	"height": 58,
	"description": "A pokemon which is green an heavy"
}
```


| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| name |    Nombre del pokemon  |  requerido |
| weigth |    Peso del pokemon  |   requerido |
| height |    Altura del pokemon  |   requerido |
| description |    Descripción del pokemon  |   opcional |


***Response***

Dependiendo el request se dará una respuesta http por defecto.

--


## Movimientos

#### CREAR MOVIMIENTO

Método : POST

EndPoint: http://localhost:8081/pokemon/{pokemon_id}/moves

***Request***
```json
{
	"name": "Agility",
	"description": "A pokemon increase its speed"
}
```


| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| name |    Nombre del movimiento  |  requerido |
| description |    Descripción del movimiento  |   opcional |


***Response***

Dependiendo el request se dará una respuesta http por defecto.


#### ACTUALIZAR MOVIMIENTO

Método : PUT

EndPoint: http://localhost:8081/pokemon/{pokemon_id}/moves/{move_id}

***Request***
```json
{
	"name": "Agility",
	"description": "A pokemon increase its speed"
}
```


| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| name |    Nombre del movimiento  |  requerido |
| description |    Descripción del movimiento  |   opcional |


***Response***

Dependiendo el request se dará una respuesta http por defecto.





#### CREAR MANERA DE APRENDIZAJE

Método : POST

EndPoint: http://localhost:8081/pokemon/{pokemon_id}/moves/{move_id}

***Request***
```json
{
	"name": "Egg Move",
	"description": "The pokemon is able to learn this move whenever its hatches the egg."
}
```


| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| name |    Nombre de la manera de aprendizaje  |  requerido |
| description |    Descripción de la manera de aprendizaje  |   opcional |


***Response***

Dependiendo el request se dará una respuesta http por defecto.


#### ACTUALIZAR Manera de Aprendizaje

Método : PUT

EndPoint: http://localhost:8081/pokemon/{pokemon_id}/moves/{move_id}/{learning_way_id}


***Request***
```json
{
	"name": "Egg Move",
	"description": "The pokemon is able to learn this move whenever its hatches the egg."
}
```


| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| name |    Nombre de la manera de aprendizaje  |  requerido |
| description |    Descripción de la manera de aprendizaje  |   opcional |


***Response***

Dependiendo el request se dará una respuesta http por defecto.



## Promedios

#### OBTENER PROMEDIOS

Método : GET

EndPoint: http://localhost:8081/averages

***Response***
```json
{
    "info": {
        "success": true
    },
    "content": {
        "average_registers": [
            {
                "id": "6002693b77f1030532aaa66f",
                "average_weigth": 96.8,
                "registers_amount": 1,
                "type": "weekly",
                "started_at": "2021-01-10T00:00:00Z",
                "ended_at": "2021-01-16T23:59:59.999Z",
                "created_at": "2021-01-16T04:19:07.765Z"
            } 
        ]
    }
}
```


| Llave   |      Descripción      |  Reglas |
|----------|:-------------:|------:|
| info |  Array que contiene la información básica del request. | - |
| info.success |    Valor que indica si el request fue exitoso o no dentro del sistema.   |   - |
| content |  Array que contiene la información del request. | - |
| content.average_registers |    Array que contiene todos los registros de promedios de pokemon.  |   - |
| content.average_registers.id |    Identificador del promedio.  |   - |
| content.average_registers.average_weigth |    Promedio de peso de pokemon registrados en el rango de fechas.  |   - |
| content.average_registers.average_height |    Promedio de altura de pokemon registrados en el rango de fechas.  |   - |
| content.average_registers.registers_amount |    Cantidad de pokemon registrados en el rango de fechas.  |   - |
| content.average_registers.type |   Tipo de promedio.  |   - |
| content.average_registers.started_at |   Fecha de inicio de promedio.  |   - |
| content.average_registers.ended_at |   Fecha de fin de promedio.  |   - |
| content.average_registers.created_at |   Fecha de creacion de promedio.  |   - |created_at


#### REGISTRAR PROMEDIO

Método : POST

EndPoint: http://localhost:8081/averages/{average_type}

average_type solamente puede ser weekly o monthly de lo contrario este no registrará el promedio.

El servicio una vez se consuma por medio de post registrará un promedio con los datos de los pokemon en el inicio fin de la semana o mes actual.

Adicional se actualizarán las ramas de los registros y se colocara la fecha en que fueron auditados.


## 3. Procesos automáticos

Para los procesos automáticos se realizo el registro de tarea en el crontab se consumirá el servicio de registrar promedio dependiendo si es semanal o mensual. 


## 4. Ambiente de prueba

Repositorio

Dentro del repositorio se encuentran 2 carpetas: 

api: En esta carpeta esta el código y el docker file para generar la imagen. 

pokemon-base-api: en esta carpeta se encuentran los archivos necesarios para levantar la base de datos en mongo y el api.
Hay una imagen en el dockerhub (chrisloops/go-pokemon-api) el cual ya tiene el api para poder levantar el ambiente sin generar la imagen.