package models

import(
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type SinglePokemon struct{
	Name string `json:"name,omitempty" validate:"required,max=50"`
	Weigth  float32 `json:"weigth,omitempty" validate:"required,numeric"`
	Height  float32 `json:"height,omitempty" validate:"required,numeric"`
	Description string `json:"description,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
	AuditedAt time.Time `bson:"audited_at,omitempty" json:"audited_at,omitempty"`
}

type SingleMove struct{
	Id primitive.ObjectID `bson:"_id"`
	Name string `json:"name,omitempty" validate:"required,max=50"`
	Description string `json:"description,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
	AuditedAt time.Time `bson:"audited_at,omitempty" json:"audited_at,omitempty"`
}

type SingleLearningWay struct{
	Id primitive.ObjectID `bson:"_id"`
	Name string `json:"name,omitempty" validate:"required,max=50"`
	Description string `json:"description,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
	AuditedAt time.Time `bson:"audited_at,omitempty" json:"audited_at,omitempty"`
}


type SingleAverageRegister struct{
	Weigth  float32 `bson:"average_weigth,omitempty" json:"average_weigth,omitempty"`
	Height  float32 `bson:"average_height,omitempty" json:"average_height,omitempty"`
	Count int `bson:"registers_amount,omitempty" json:"registers_amount,omitempty"`
	Type string `bson:"type,omitempty" json:"type,omitempty"`
	StartedAt time.Time `bson:"started_at,omitempty" json:"started_at,omitempty"`
	EndedAt time.Time `bson:"ended_at,omitempty" json:"ended_at,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
}