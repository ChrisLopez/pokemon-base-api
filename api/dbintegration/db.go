package dbintegration


import (
	"context"
	"log"
	"time"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"fmt"
	"strings"
	"os"
 )
 
 func GetUrl()(string){
	url := os.Getenv("MONGO_URL")
    port := os.Getenv("MONGO_PORT")
    user := os.Getenv("MONGO_USER")
    password := os.Getenv("MONGO_PASS")
	db := os.Getenv("MONGO_DB")
	

	
	arrayconeection := []string{"mongodb://", user, ":",password,"@", url,":",port,"/",db,"?authSource=",db };
	mongourl:=	strings.Join(arrayconeection , "")
	return mongourl
 }

 func Connect() ( *mongo.Client, error){
	 
    url := os.Getenv("MONGO_URL")
    port := os.Getenv("MONGO_PORT")
    user := os.Getenv("MONGO_USER")
    password := os.Getenv("MONGO_PASS")
	db := os.Getenv("MONGO_DB")
	

	
	arrayconeection := []string{"mongodb://", user, ":",password,"@", url,":",port,"/",db,"?authSource=",db };
	mongourl:=	strings.Join(arrayconeection , "")
	fmt.Printf(mongourl);
	
//	 client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://chrisloops:K3sqv7rKsz5azFBs@127.0.0.1:27017/pokemon?authSource=pokemon"))
	 client, err := mongo.NewClient(options.Client().ApplyURI(mongourl))

	 if err != nil {
		 log.Fatal(err)
		 return nil, err
	 }

	 ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	 err = client.Connect(ctx)
	 if err != nil {
		log.Fatal(err)
		return nil, err
	 }


	 //defer client.Disconnect(ctx)
	 return client, nil
}
/*
func Insert(){
	client, err := Connect()

	if(err != nil){
		log.Print("Something whent wrong")
		return 
	}
	pokemonList := []restclient.Pokemon{
        restclient.Pokemon{Name:"Charmander",Weigth:55.5,Height:25.00,Description:"A pokemon which is red", Moves : []restclient.Move{ restclient.Move{Name:"Iron Tail",Description:"The pokemon tacle the oponent with its tail", LearningWays : []restclient.LearningWay{ restclient.LearningWay{Name:"By Level", Description:"The pokemon is able to learn this movement whenever it raises the level 30."}}}}, },
		restclient.Pokemon{Name:"Squirtle",Weigth:55.5,Height:25.00, Description:"A pokemon which is blue"},
	} 

	pokemon := restclient.Pokemon{Name:"Bulbasaur",Weigth:55.5,Height:25.00,Description:"A pokemon which is green", Moves : []restclient.Move{ restclient.Move{Name:"Iron Tail",Description:"The pokemon tacle the oponent with its tail", LearningWays : []restclient.LearningWay{ restclient.LearningWay{Name:"By Level", Description:"The pokemon is able to learn this movement whenever it raises the level 30."}}}}, }
		
	pokemonCollection := client.Database("pokemon").Collection("pokemon")

	insertResult, err := pokemonCollection.InsertOne(context.TODO(), pokemon)



	if err != nil {

		log.Fatal(err)

	}

	fmt.Println("Inserted post with ID:", insertResult.InsertedID)

}
*/