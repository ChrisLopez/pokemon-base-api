package utils

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"github.com/go-playground/validator/v10"
	"chrisloops.com/api/models"
)

func ParseBody(r *http.Request, x interface{}) {
	if body, err := ioutil.ReadAll(r.Body); err == nil {
		if err := json.Unmarshal([]byte(body), x); err != nil {
			return
		}
	}
}

func ResponseBadRequest(w http.ResponseWriter, errors []validator.FieldError){

	errorString := []string{}
	
	for _, e := range errors {
		errorString = append(errorString, e.Error())
	}

	response := models.Response {
		Info : models.Info{Success: false, Title: "There's was something wrong", Message: "Please check the data in your request."},
		Content: models.ValidationErrors{Errors: errorString },
	}
	
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(response)
}

func ResponseOk(w http.ResponseWriter, title string, message string){

	response := models.Response {
		Info : models.Info{Success: true, Title: title, Message: message},
	}
	
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func ResponseJson(w http.ResponseWriter, response interface{}){
	w.Header().Set("Content-Type", "application/json")	
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}

func ResponseNotFound(w http.ResponseWriter, title string, message string){

	response := models.Response {
		Info : models.Info{Success: false, Title: title, Message: message},
	}
	
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	json.NewEncoder(w).Encode(response)
}

func ResponseServerError(w http.ResponseWriter){
	response := models.Response {
		Info : models.Info{Success: true, Title: "We are sorry", Message: "There was an error which we can't handle right now, please try again later."},
	}
	
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(response)
}

