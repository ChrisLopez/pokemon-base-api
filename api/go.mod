module chrisloops.com/api

go 1.15

require (
	github.com/go-playground/validator/v10 v10.4.1
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/now v1.1.1
	go.mongodb.org/mongo-driver v1.4.4
)
